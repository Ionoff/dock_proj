import os


def calculate_power(a, n):
    if n == 0:
        return 1
    result = 1
    for _ in range(int(n)):
        result *= a
    return result


if __name__ == "__main__":
    a = float(os.environ.get("A", 0.0))
    n = float(os.environ.get("N", 0.0))

power_result = calculate_power(a, n)
print(f"{a} в степени {n} равно {power_result}")
