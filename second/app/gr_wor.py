import mysql.connector


def connect_to_db():
    try:
        connection = mysql.connector.connect(
            host="localhost",
            user="your_username",
            password="your_password",
            database="dock_proj_db"
        )
        return connection
    except mysql.connector.Error as error:
        print(f"Error: {error}")


def get_students_info(connection):
    cursor = connection.cursor()
    query = "SELECT student_id, student_fn, student_ln, student_bd FROM stud_group"
    cursor.execute(query)
    result = cursor.fetchall()

    print(f"{'Student ID':<10} {'ФИО':<20} {'Дата рождения':<10}")
    for row in result:
        student_id, student_fn, student_ln, student_bd = row
        print(f"{student_id:<10} {student_fn} {student_ln:<20} {student_bd:<10}")


def main():
    connection = connect_to_db()
    if connection.is_connected():
        get_students_info(connection)
        connection.close()


if __name__ == "__main__":
    main()