CREATE DATABASE IF NOT EXISTS dock_proj_db;
USE dock_proj_db;
CREATE TABLE IF NOT EXISTS stud_group
(
student_id INTEGER PRIMARY KEY,
student_fn VARCHAR(50) NOT NULL,
student_ln VARCHAR(50) NOT NULL,
student_bd DATE NOT NULL
);

CREATE TABLE IF NOT EXISTS record_book
(
    record_book_id serial PRIMARY KEY,
    student_id INTEGER NOT NULL,
    discipline VARCHAR(50) NOT NULL,
    exam_date VARCHAR(50) NOT NULL,
    teacher VARCHAR(50) NOT NULL,
    mark INTEGER NOT NULL,
    FOREIGN KEY (student_id) REFERENCES stud_group (student_id) ON DELETE CASCADE
);

INSERT IGNORE INTO stud_group (student_id, student_fn, student_ln, student_bd)
VALUES
(1, 'Ivan', 'Shorohov', '2002-10-21'),
(2, 'Maria', 'Agapova', '2001-08-12'),
(3, 'Vasya', 'Trupov', '2002-06-28'),
(4, 'Evgenya', 'Topova', '2001-12-23'),
(5, 'Lena', 'Uhorina', '2002-09-29'),
(6, 'Dmitriy', 'Valyanov', '2002-07-19'),
(7, 'Yarik', 'Potopov', '2001-11-17'),
(8, 'Katya', 'Molchanova', '2003-09-07'),
(9, 'Daniil', 'Andreev', '2002-04-21'),
(10, 'Petr', 'Perviy', '1672-06-09');

INSERT IGNORE INTO record_book (student_id, discipline, exam_date, teacher, mark)
VALUES
(1, 'Informatics', '2024-01-29', 'Egorov Zahar', 5),
(1, 'Physics', '2024-01-21', 'Vazova Elena', 3),
(1, 'History', '2024-01-14', 'Morozova Alla', 4),
(1, 'Math', '2024-01-08', 'Ogorodov Aleksey', 5),
(5, 'Math', '2024-01-08', 'Ogorodov Aleksey', 4),
(4, 'Math', '2024-01-08', 'Ogorodov Aleksey', 3),
(3, 'Math', '2024-01-08', 'Ogorodov Aleksey', 4),
(2, 'Math', '2024-01-08', 'Ogorodov Aleksey', 4),
(6, 'Math', '2024-01-08', 'Ogorodov Aleksey', 5),
(7, 'Math', '2024-01-08', 'Ogorodov Aleksey', 3),
(8, 'Math', '2024-01-08', 'Ogorodov Aleksey', 2),
(9, 'Math', '2024-01-08', 'Ogorodov Aleksey', 4),
(10, 'Math', '2024-01-08', 'Ogorodov Aleksey', 5),
(2, 'History', '2024-01-14', 'Morozova Alla', 4),
(3, 'History', '2024-01-14', 'Morozova Alla', 4),
(4, 'History', '2024-01-14', 'Morozova Alla', 5),
(5, 'History', '2024-01-14', 'Morozova Alla', 4),
(6, 'History', '2024-01-14', 'Morozova Alla', 5),
(7, 'History', '2024-01-14', 'Morozova Alla', 5),
(8, 'History', '2024-01-14', 'Morozova Alla', 5),
(9, 'History', '2024-01-14', 'Morozova Alla' ,4),
(10, 'History', '2024-01-14', 'Morozova Alla', 3),
(2, 'Physics', '2024-01-21', 'Vazova Elena', 3),
(3, 'Physics', '2024-01-21', 'Vazova Elena', 5),
(4, 'Physics', '2024-01-21', 'Vazova Elena', 3),
(5, 'Physics', '2024-01-21', 'Vazova Elena', 3),
(6, 'Physics', '2024-01-21', 'Vazova Elena', 4),
(7, 'Physics', '2024-01-21', 'Vazova Elena', 4),
(8, 'Physics', '2024-01-21', 'Vazova Elena', 3),
(9, 'Physics', '2024-01-21', 'Vazova Elena', 4),
(10, 'Physics' ,'2024-01-21', 'Vazova Elena', 3),
(2, 'Informatics', '2024-01-29', 'Egorov Zahar', 5),
(3, 'Informatics', '2024-01-29', 'Egorov Zahar', 4),
(4, 'Informatics', '2024-01-29', 'Egorov Zahar', 3),
(5, 'Informatics', '2024-01-29', 'Egorov Zahar', 4),
(6, 'Informatics', '2024-01-29', 'Egorov Zahar', 4),
(7, 'Informatics', '2024-01-29', 'Egorov Zahar' ,5),
(8, 'Informatics', '2024-01-29', 'Egorov Zahar', 5),
(9, 'Informatics', '2024-01-29', 'Egorov Zahar', 5),
(10, 'Informatics', '2024-01-29', 'Egorov Zahar' ,4);